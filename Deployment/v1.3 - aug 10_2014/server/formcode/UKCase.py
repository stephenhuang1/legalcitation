import re
import sys
from Functions import *


'''****************     STYLE OF CAUSE     ****************'''

#this function is in UKCase ONLY
def regstrSpec(i):#i is a string input
	one = r'\s'+i+ r'(?=,)'
	two = r'\s'+i+ r'$'
	thr = r'\s'+i+ r'\s'
	fou = r'^' +i+ r'\s'
	fiv = r'\('+i+ r'\)'
	six = r'\['+i+ r'\]'
	string =  r'('+ one + r'|' + two + r'|' + thr + r'|' + fou + r'|' + fiv + r'|' + six + r')'
	return string



'''****************     CITATIONS     ****************'''
'''****************     CITATIONS     ****************'''
'''****************     CITATIONS     ****************'''

def Divide(Citation_Input):
	PC = CleanUp(Citation_Input)
	if re.search(r"(;|,)$", PC):
		PC = CleanUp(PC[:-1])
	if re.search(r"^(;|,)", PC):
		PC = CleanUp(PC[1:])
	m = re.split('[,;]', PC) # 	#Split the citations based on positioning of commas and semicolons
	if type(m)!=list:
		m = [m]
	return m



#takes in a the citaion input string
#returns: [string, "NC"/"EWHC"/"No NC"]
#Court must be surrounded by a space on each side
def CheckNC(Citation_Input): #pull the neutral citation from the list if there is one
	#print "**** Starting CheckNC"
	#print "Checking for NC in string: ", Citation_Input
	m = Divide(Citation_Input)
	#print "List of reporters: ", m
	for string in m: 
		Year = PullDate(string)
		if Year:
			yr = re.search(regstrElec(Year), string)
			if yr:
				string = CleanUp("["+Year+"] " + re.sub(yr.group(), " ", string))
		else: 
			string = CleanUp("[input year] " + string)
		#print "string modified to: ", string
		Courts = ['UKHL', 'UKPC', 'EWCA Civ', 'EWCA Crim', 'EWHC Admin']
		for x in Courts:
			if re.search(regstrElec(x), string, re.I): 
				#print "Found neutral citation: ", x, "returning: ", string, '\n'
				return [string, "NC"]
		if re.search(regstrElec("EWHC"), string, re.I):
			EWHCDivs = [['(Ch)', re.compile(r'\(?Ch(ancery)?( Div)?(ision)?\)?', flags = re.I)], ['(Pat)', re.compile(r'\(?Pat(ents)?\s?(Ct|Court)?\)?', flags = re.I)], ['(QB)', re.compile(r"\(?(QB|Queen's Bench|Queens Bench)( Div)?(ision)?\)?", flags = re.I)], ['(Admin)', re.compile(r'\(?Admin(istrative)?\s?(Ct|Court)?\)?', flags = re.I)], ['(Comm)', re.compile(r'\(?Comm(ercial)?\s?(Ct|Court)?\)?', flags = re.I)], ['(Admlty)', re.compile(r'\(?(Admirality|Admir|Admlty)\s?(Ct|Court)?\)?', flags = re.I)], ['(TCC)', re.compile(r'\(?(TCC|Technology and Construction|Technology & Construction|Tech and Constr|Tech & Constr)\s?(Ct|Court)?\)?', flags = re.I)], ['(Fam)', re.compile(r'\(?Fam(ily)?( Div)?(ision)?\)?', flags = re.I)]]
			for x in EWHCDivs:
				match = x[1].search(string)
				if match:
					#print match.group()
					string = CleanUp(re.sub(match.group(), " ", string) + " " +x[0])
					string = CleanUp(re.sub(r'\(\s\)', ' ', string))
					#print "Found neutral citation: ", x, "\nreturning: ", string, '\n'
					return [string, "NC"]
			#print "returning: ", [string, "EWHC"], '\n'
			return [string, "EWHC"]	
		Scot = ['HCJT', 'HCJAC', 'CSOH', 'CSIH']
		for x in Scot:
			if re.search(regstrElec(x), string, re.I): 
				#print "Found neutral citation: ", x, "returning: ", string, '\n'
				return [string, "NC"]
	return [string, "No NC"]
	

#returns [string, "none", "dropdown"/"court"/"no court"]
#true if there is an instance of LR or Law Reports --- have a drop down menu
#drop down menu should have ["Admirality and Ecclesiastical Cases (A & E)", "Appeal Cases (AC)", "Chancery (Ch)", "Chancery Appeals (Ch App)", "Common Pleas (CP)", "Crown Cases Reserved (CCR)", "Equity Cases (Eq)", "Exchequer (Ex)", "English and Irish Appeal Cases (HL)", "Family (Fam)", "Industrial Courts Reports (ICR)", "Ireland (Ir)", "King's Bench (KB)", "Privy Council (PC)", "Probate (P)", "Queen's Bench (QB)", "Law Reports Restrictive Practices (LR RP)", "Scotch and Divorce Appeal Cases (Sc & Div)"]
def LawReports(Citation_Input):
	#print "\n******** Starting LawReports **********"
	#print "Citation Input: ", Citation_Input
	m = Divide(Citation_Input)
	#print "List of reporters: ", m
	Abbs = ['A & E', 'AC', 'Ch', 'Ch App', 'CP', 'CCR', 'Eq', 'Ex', 'HL', 'Fam', 'ICR', 'Ir', 'KB', 'PC', 'P', 'QB', 'RP', 'Sc & Div']
	for s in m:
		#print "s = ", s
		string = CleanUp(re.sub("\sLR\s?", " ", s, flags = re.I))
		string = CleanUp(re.sub("\slaw reports?\s?", " ", string, flags = re.I))
		#print "string = ", string
		for x in Abbs:
			if re.search(regstr(x), string, re.I):
				string = re.sub(re.search(regstr(x), string, re.I).group(), " "+x+' ', string)
				Year = PullDate(string)
				#print "got a hit: ", string
				if Year:
					yr = re.search(regstrElec(Year), string)
					if yr:
						string = CleanUp("["+Year+"] " + re.sub(yr.group(), " ", string))
				else: 
					string = CleanUp("[input year] " + string)
				if (x =="HL") or (x=="PC"):
					#print "returning: ", [string, "court"]
					return [string, "court"]
				else: 
					#print "returning: ", [string, "no court"]
					return [string, "no court"]
		#if there is no proper instance of the law reports, see if the words LR or Law Reports are mentioned, and if so have a drop down menu
		matchone = re.search(regstr("LR"), s, re.I)
		matchtwo = re.search(regstr("Law Reports"), s, re.I)
		if matchone or matchtwo:
			#print "returning", [s, "LRdropdown"]
			return [s, "LRdropdown"]
	#print "returning", [m, "none"]
	return [m, "none"]

'''#returns [string, "court"/"no court"]
def InterpretLRInput(string):#how to do it when Law Reports (LR) are used
	#print "******** Starting InterpretLRInput **********"
	#print "Input: ", string
	KeepAsIs = [["Appeal Cases (AC)", 'AC'], ["Chancery (Ch)", "Ch"],["Common Pleas (CP)", "CP"], ["Exchequer (Ex)", "Ex"], ["Family (Fam)", "Fam"], ["Industrial Courts Reports (ICR)", "ICR"], ["King's Bench (KB)", "KB"], ["Probate (P)", "P"], ["Queen's Bench (QB)", "QB"], ["Law Reports Restrictive Practices (LR RP)", "RP"]]
	Change = [["Admirality and Ecclesiastical Cases (A & E)", 'A & E'], ["Chancery Appeals (Ch App)", 'Ch App'], ['Crown Cases Reserved (CCR)', 'CCR'], ['Equity Cases (Eq)', 'Eq'], ['English and Irish Appeal Cases (HL)', 'HL'], ['Ireland (Ir)', 'Ir'], ['Privy Council (PC)', 'PC'], ['Scotch and Divorce Appeal Cases (Sc & Div)', 'Sc & Div']]
	for x in KeepAsIs:
		if x[0]==string:
			return [x[1], "no court"]
	for x in Change:
		if x[0]==string:
			if (x[1] == "LRHL") or (x[1] == "LRPC"):
				return [string, "court"]
			return [x[1], "no court"]'''


#returns [best reporter, "court"/"no court"]
#input the citation input and whether there is a Neutral Citation (NC). NC = True or False
def BestReporter(Citation_Input):
	#print "******** Starting BestReporter **********"
	#print "Citation Input: ", Citation_Input
	m = Divide(Citation_Input)
	#print "List of reporters: ", m				
	series = ["2d", "3d", "4th", "5th", "6th", "7th", "8th"]
	for x in range(len(m)): #replace "2d" with "(2d)", etc (i.e. put them in brackets
		for s in series:
			match = re.search(' '+s+' ', m[x], re.I)
			if match:
				#print "Found a series number without brackets"
				m[x] = re.sub(match.group(), ' ('+s+') ', m[x])
				break
	for x in range(len(m)): m[x] = CleanUp(m[x]) #remove excess white spaces on either side
	#print "List of reporters: ", m
	#print "Checking LawReports..."
	for x in m:
		LR = LawReports(x)
		#print "\nBack in BestReporter. LawReports returned: ", LR
		if (LR[1]=="court") or (LR[1]=="no court"):
			#print "Affirmative there is a law report. returning: ", LR, "\n"
			return LR
	#print "Nothing Found in Law Reports, checking now in ER and other Reporters...."
	ER = ["ER"]
	Reporters = ['A & N', 'AC', 'Adam', 'Add', 'ADIL', 'Al', 'All ER', 'All ER (Comm)', 'All ER (EC)', 'All ER Rep', 'All ER Rep Ext', 'ALR', 'Amb', 'And', 'Andr', 'Anst', 'App Cas', 'App Div', 'Arn', 'Arn & H', 'Asp MLC', 'Atk', 'B & Ad', 'B & Ald', 'B & CR', 'B & Cress', 'B & S', 'Ball & B', 'Barn C', 'Barn KB', 'Barnes', 'Batt', 'Beat', 'Beav', 'Bel', 'Bell', 'Ben & D', 'Benl', 'BILC', 'Bing', 'Bing NC', 'BISD', 'Bla H', 'Bla W', 'Bli', 'Bli NS', 'Bos & Pul', 'Bos & Pul NR', 'Bridg', 'Bridg Conv', 'Bridg J', 'Bridg O', 'Bro CC', 'Bro PC', 'Brod & Bing', 'Brooke NC', 'Brown & Lush', 'Brownl', 'Bulst', 'Bunb', 'Burr', 'Burrell', 'C & J', 'C & M', 'Calth', 'Camp', 'Car & K', 'Car & M', 'Car & P', 'Carter', 'Carth', 'Cary', 'Cas t Hard', 'Cas t Talb', 'CB', 'CB (NS)', 'Ch', 'ChApp', 'Ch Ca', 'Ch D', 'Ch R', 'Chan Cas', 'Chit', 'Choyce Ca', 'CIJ M\\xe9moires', 'CIJ Rec', 'Cl & F', 'CM & R', 'Coll', 'Colles', 'Corn', 'Comb', 'Cooke CP', 'Coop Pr Ca', 'Coop t Br', 'Coop t Cott', 'Coop G', 'Co Rep', 'Cowp', 'Cox', 'CPD', 'CPJI (Ser A)', 'CPJI (S\\xe9r B)', 'CPJI (S\\xe9r A/B)', 'CPJI (S\\xe9r C)', 'Cun', 'Curt', 'Dan', 'Davis', 'Dee & Sw', 'Dears', 'Dears & B', 'De G & J', 'De G & Sm', 'De G F & J', 'De G J & S', 'De G M & G', 'Den', 'Dick', 'Dods', 'Donn', 'Doug', 'Dow', 'Dow & Cl', 'Dowl & Ry', 'Drew', 'Drew &', 'Dy', 'East', 'Eden', 'Edw', 'El & Bl', 'El & El', 'El Bl & El', 'EMLR', 'Eq Ca Abr', 'ER', 'Esp', 'Ex D', 'Exch Rep', 'F', 'F & F', 'Fam', 'Fitz-G', 'Forrest', 'Fort', 'Fost', 'FTLR', 'Giff', 'Gilb Cas', 'Gilb Rep', 'Godbolt', 'Gould', 'Gow', 'H&C', 'H&M', 'H&N', 'H & Tw', 'Hag Adm', 'Hag Con', 'Hag Ecc', 'Hague Ct Rep', 'Hague Ct Rep (2d)', 'Hardr', 'Hare', 'Hay & M', 'Her Tr Nor', 'Het', 'HL Cas', 'HL Cas', 'Hob', 'Hodges', 'Holt', 'Holt, Eq', 'HoIt, KB', 'Hut', 'IBDD', 'I Ch R', 'ICJ Pleadings', 'ICJ Rep', 'ICLR', 'ICR', 'ICR', 'ICSID', 'I LR', 'I LR', 'ILRM', 'ILTR', 'Inter-Am Ct HR (Ser A)', 'Inter-Am Ct HR (Ser B)', 'Inter-Am Ct HR (Ser C)', 'IR', 'IR Eq', 'I RCL', 'J&H', 'Jac', 'Jac & W', 'Jenk', 'Johns', 'Jones, T', 'Jones, W', 'K & J', 'Kay', 'KB', 'Keble', 'Keen', 'Keilway', 'Kel J', 'Kel W', 'Keny', 'Kn', 'Lane', 'Latch', 'Leach', 'Lee', 'Leo', 'Lev', 'Lewin', 'Le & Ca', 'Ley', 'Lilly', 'Lit', 'LI LR', "Lloyd's LR", "Lloyd's Rep", "Lloyd's Rep Med", 'Lush', 'Lut', 'M&M', 'M & Rob', 'M&S', 'M &W', 'Mac & G', 'MacI & R', 'Madd', 'Man & G', 'March, NR', "M'Cle", "M'Cle & Yo", 'Mer', 'Mod', 'Moo Ind App', 'Moo KB', 'Moo PC', 'Moo PCNS', 'Mood', 'Mos', 'My & Cr', 'My & K', 'Nels', 'Noy', 'Ow', 'P', 'P Wms', 'Palm', 'Park', 'PD', 'Peake', 'Peake Add Cas', 'Ph', 'Phill Ecc', 'PI Com', 'Pollex', 'Pop', 'Prec Ch', 'Price', 'QB', 'QBD', 'Raym Ld', 'Raym T', 'Rep Ch', 'Rep t Finch', 'Ridg t Hard', 'RIAA', 'Rob / Rob Chr', 'Rob Ecc', 'Rolle', 'RPC', 'RPC', 'RR', 'Russ', 'Russ & M', 'Russ & Ry', 'Salk', 'Sav', 'Say', 'Scot LR', 'Sel Cat King', 'Sess Cas', 'Sess Cas', 'Sess Cas S', 'Sess Cas D', 'Sess Cas F', 'Sess Cas M', 'Sess Cas R', 'Show KB', 'Show PC', 'Sid', 'Sim', 'Sim (NS)', 'Sim & St', 'SLT', 'Skin', 'Sm & G', 'Sp Ecc & Ad', 'Sp PC', 'Stark', 'Str', 'Sty', 'Sw & Tr', 'Swab', 'Swans', 'Talb', 'Taml', 'Taun', 'TLR', 'Toth', 'TR', 'TTC', 'TTR (2d)', 'Vaugh', 'Vent', 'Vern', 'Ves & Bea', 'Ves Jr', 'Ves Sr', 'Welsb H & G', 'West, t Hard', 'West', 'Wight', 'Will Woll & H', 'Willes', 'Wilm', 'Wils Ch', 'Wils Ex', 'Wils KB', 'Winch', 'WLR', 'Wms Saund', 'W Rob', 'Y & C Ex', 'Y & CCC', 'Y & J', 'Yel', 'You']
	F = CheckReporter(m, ER)
	if F:
		#print "Found ER: ", F[0]
		return [F[0], "no court"]
	F = CheckReporter(m, Reporters)
	if F:
		#print "Found a reporter: ", F[0]
		return [F[0], "no court"]
	#print "No reporters found, returning default (the first court in m, being ", m[0]
	return [m[0], "no court"]#return the input if it's not recognized
	
	
#COURT WILL BE A DROP DOWN MENU CONTAINING THE FOLLOWING
'''[["Chancery Court", "Ch"], 
["Court of Justice (Scotland)", "Ct Just"], 
["Court of Sessions (Scotland)", "Ct Sess"], 
["High Court of Admirality", "HC Adm"], 
["High Court of Justice", "HCJ"], 
["High Court: Chancery Division", "ChD"],
["High Court: Family Division", "FamD"], 
["High Court: Queen's Bench Division", "QBD"], 
["High Court: Divisional Court", "Div Ct"], 
["Court of Appeal: Criminal Division", "CA Crim"],
["Court of Appeal: Civil Division", "CA"], 
["House of Lords (England)", "HL (Eng)"],
["House of Lords (Scotland)", "HL (Scot)"],
["Judicial Committee of the Privy Council", "PC"],
["Stipendiary Magistrate Court", "Stip Mag Ct"],
["Magistrate Court", "Mag Ct"],
["Crown Court", "Crown Ct"],
["Stipendiary Magistrate Court", "Stip Mag Ct"],
["Other", "Other"]]#(in case of other, have them input something and run DefaultCt on it'''




def AutoPCPinpoint(Citation_Input):
	#print "\n****** AutoPCPinpoint"
	#print "input is:\n", "citation string: ", Citation_Input
	m = Divide(Citation_Input)
	NC = CheckNC(Citation_Input)
	if NC[1]=="NC":# or (NC[1]=="EWHC"):#returns: [string, "NC"/"EWHC"/"No NC"/]
		neut = re.sub(regstrSpec(r'\d+'), '', NC[0])
		if len(m)==1:
			return ["Warning: should have reporter", neut]
		return ["neutral", neut]
	if NC[1]=="EWHC":
		return ["EWHC", NC[0]]
	R = BestReporter(Citation_Input)[0]
	report = CleanUpTitle(re.sub(regstrSpec(r'\d+'), '', R))
	return ["reporter", report]

	
#If CheckNC: need to cite to para
#otherwise cite to page or para in the reporter from BestReporter
#pincite = False or ["pinPoint_para"/"pinPoint_page", para or page number input]
def GetCitations(Citation_Input, Court_Input, Date_Input, pincite):
	#print "\n****** Starting GetCitations"
	#print "citation string: ", Citation_Input, "\n", "court: ", Court_Input, "\n", "date: ", Date_Input, "\n", "pincite: ", pincite, "\n"
	if not Citation_Input:
		return "ERROR: missing citation input"
	if not Court_Input:
		return ", ERROR: missing court input"
	if not Date_Input:
		return ", ERROR: missing date input"
	NC = CheckNC(Citation_Input) #returns: [string, "NC"/"EWHC"/"No NC"] #pull the neutral citation from the list if there is one
	if (NC[1]=="NC") or (NC[1]=="EWHC"):
		#print "In GetCitations, found NC:", NC[0]
		NeutralCitation = NC[0]
	else: NeutralCitation = False
	Auto = AutoPCPinpoint(Citation_Input)
	citestr = ""
	if pincite:
		if pincite[0]=="pinPoint_para":
			citestr = " at para " + pincite[1]
		elif pincite[0] == "pinPoint_page":
			citestr = " at " + pincite[1]
	#find if there is a citationyear present. if it is in the neutral citation, format it correctly
	CitationYear = False #assume there is no citation date evident in the input
	Court = False #first assume there is no court evident in the reporter
	JudgementYear = False #assume there is no date evident in the input judgement
	BR = BestReporter(Citation_Input) #returns the best reporter, no funny business
	Best = BR[0]
	if NeutralCitation: 
		Court = True
		CitationYear = PullDate(NeutralCitation)
		if not CitationYear:
			NeutralCitation = CleanUp(NeutralCitation + citestr)
			CitationYear = True
		JudgementYear = CitationYear
		m = Divide(Citation_Input)
		if (len(m)==1):
			return ', ' + NeutralCitation
	else: CitationYear = PullDate(Best)
	if Auto[1] == "reporter":
		Best += citestr
	if BR[1] == "court":
		Court = True
	if PullDate(Best): 
		CitationYear = PullDate(Best) #set the citation date to be the date in the string, if present
	#print "Court = ", Court #True or False
	#print "Citation Date = ", CitationYear #year or False or True
	if not Court and not CitationYear:
		#print "NOT COURT AND NOT CITATIONDATE DETECTED ****"
		Ct = DefaultCt(CleanUp(Court_Input))
		JudgementYear = PullDate(CleanUp(Date_Input))
		OUTPUT = ' ('+ JudgementYear + '), ' + Best + ' ' + Ct#combine all of this in the right way
	if Court and not CitationYear:
		#print "COURT AND NOT CITATIONDATE DETECTED ****"
		JudgementYear = PullDate(CleanUp(Date_Input))
		OUTPUT = ' ('+ JudgementYear + '), ' + Best#combine all of this in the right way
	if CitationYear and not Court: 
		#print "CITATIONDATE AND NOT COURT DETECTED ****"
		Ct = DefaultCt(CleanUp(Court_Input))
		JudgementYear = PullDate(CleanUp(Date_Input))
		OUTPUT = ' ('+ JudgementYear + '), ' + Best + ' ' + Ct#combine all of this in the right way
		if (JudgementYear==CitationYear): 
			OUTPUT = ', ' + Best + ' (' + Ct + ')'
		else:
			OUTPUT = ' ('+ JudgementYear + '), ' + Best + ' (' + Ct+ ')'
	if NeutralCitation:
		#print "CITATIONDATE AND COURT DETECTED"
		OUTPUT = ", " + NeutralCitation + citestr+', ' + Best
	#print "Result:", OUTPUT
	return OUTPUT

#GetCitations("2004 EWHC 1974 (Commercial), 2004 2 LRAC 457", "ukhl", "2004", False)


	
'''****************     HISTORY     ****************'''


#this is the function that will ultimately call all of the other functions for the parallel citations
#the input is what is written in the form for parallel citations
def GetHistoryCitations(Citation_Input, Date_Input, Court_Input):
	#print "\n****** Starting GetHistoryCitations"
	#print "citation string: ", Citation_Input, "\n", "court: ", Court_Input, "\n", "date: ", Date_Input, "\n"
	if not Citation_Input:
		return ", ERROR: missing citation input"
	if not Court_Input:
		return ", ERROR: missing court input"
	if not Date_Input:
		return ", ERROR: missing date input"
	#First, look to see if there is a neutral citaiton. If so, return it.
	NC = CheckNC(Citation_Input) #returns: [string, "NC"/"EWHC"/"No NC"] #pull the neutral citation from the list if there is one
	if (NC[1]=="NC") or (NC[1]=="EWHC"):
		#print "In GetCitations, found NC:", NC[0]
		NeutralCitation = NC[0]
		CitationYear = PullDate(NeutralCitation)
		if not CitationYear:
			NeutralCitation = ", [input year] " + CleanUp(NeutralCitation)
		#print "returning: ", NeutralCitation
		return NeutralCitation
	#Second, since there is no neutralcitation, we simply return the best reporter, correctly formatted
	BR = BestReporter(Citation_Input) #returns the best reporter, no funny business
	Best = BR[0]
	CitationYear = False #assume there is no citation date evident in the input
	Court = False #first assume there is no court evident in the reporter
	JudgementYear = False #assume there is no date evident in the input judgement
	if BR[1] == "court":
		Court = True
	if PullDate(Best): 
		CitationYear = PullDate(Best) #set the citation date to be the date in the string, if present
	#print "Court = ", Court #True or False
	#print "Citation Date = ", CitationYear #year or False or True
	if not Court and not CitationYear:
		#print "NOT COURT AND NOT CITATIONDATE DETECTED ****"
		Ct = DefaultCt(CleanUp(Court_Input))
		JudgementYear = PullDate(CleanUp(Date_Input))
		OUTPUT = ' ('+ JudgementYear + '), ' + Best + ' ' + ' (' + Ct+')'#combine all of this in the right way
	if Court and not CitationYear:
		#print "COURT AND NOT CITATIONDATE DETECTED ****"
		JudgementYear = PullDate(CleanUp(Date_Input))
		OUTPUT = ' ('+ JudgementYear + '), ' + Best#combine all of this in the right way
	if CitationYear and not Court: 
		#print "CITATIONDATE AND NOT COURT DETECTED ****"
		Ct = DefaultCt(CleanUp(Court_Input))
		JudgementYear = PullDate(CleanUp(Date_Input))
		OUTPUT = ' ('+ JudgementYear + '), ' + Best + ' ' + Ct#combine all of this in the right way
		if (JudgementYear==CitationYear): 
			OUTPUT = ', ' + Best + ' (' + Ct + ')'
		else:
			OUTPUT = ' ('+ JudgementYear + '), ' + Best + ' (' + Ct+ ')'
	#print "returning: ", OUTPUT
	return OUTPUT


def GetHistory(listoflists):
	#[affirming/reversing, parallel, year, court]
	List = []
	for Instance in listoflists:
		if re.search("affirming", CleanUp(Instance[0]), re.I):
			List.append(", aff'g"+ GetHistoryCitations(Instance[1], Instance[2], Instance[3]))
		if re.search("reversing", CleanUp(Instance[0]), re.I):
			List.append(", rev'g"+ GetHistoryCitations(Instance[1], Instance[2], Instance[3]))
		if re.search("affirmed", CleanUp(Instance[0]), re.I):
			List.append(", aff'd"+ GetCitations(Instance[1], Instance[3], Instance[2], False))
		if re.search("reversed", CleanUp(Instance[0]), re.I):
			List.append(", rev'd"+ GetCitations(Instance[1], Instance[3], Instance[2], False))
	output = ""
	for x in List:
		output = output + x
	return output


'''****************     CITING     ****************'''

def GetCiting(SoC, Parallel, Year, Court):
	SoC = GetStyleOfCause(SoC)
	Citation = GetCitations(Parallel, Court, Year, False)
	return ", citing "+SoC + Citation
	

'''****************     LEAVE TO APPEAL     ****************'''

def GetLeaveToAppeal(array):
	#[granted/requested/refused/asofright, court, citation/or docketnumber]
	if re.search("Requested", CleanUp(array[0]), re.I):
		return ", leave to appeal to " + Court + " requested"
	if re.search("Granted", CleanUp(array[0]), re.I):
		return ", leave to appeal to " + Court + " granted, " + str(array[2])
	if re.search("Refused", CleanUp(array[0]), re.I):
		return ", leave to appeal to " + Court + " refused, " + str(array[2])
	if re.search("AsofRight", CleanUp(array[0]), re.I):
		return ", appeal as of right to " + Court	
	return ", sorry error in leave to appeal option"
	
	
'''****************     SHORT FORM     ****************'''
	
def GetShortForm(string):
	return " [<i>"+Capitalize(string)+"</i>]"


'''****************     JUDGE    ****************'''

#need to have some front-end searching to find J or JJ, etc
#need to know if it's dissenting
def GetJudge(string, dissenting):
	string = Capitalize(string)
	if dissenting:
		string = string + ", dissenting"
	return ", " + string
	

class UKCaseClass(object):
	def __init__(self):
		self.GetCitations = GetCitations
		self.GetStyleOfCause = GetStyleOfCause
		self.GetHistory = GetHistory
		self.GetCiting = GetCiting
		self.GetLeaveToAppeal = GetLeaveToAppeal
		self.GetShortForm = GetShortForm
		self.GetJudge = GetJudge
		self.AutoPCPinpoint = AutoPCPinpoint
		self.PullDate = PullDate
	
	
	
